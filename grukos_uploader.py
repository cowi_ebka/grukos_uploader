#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
This application verifies and uploads shapefiles with groundwater-related data
to the GRUKOS database.
'''
import sys
import os
import subprocess
import argparse
import unittest
import shapefile    # pip install pyshp
import wx           # pip install wxPython (slow)/conda install wxPython (fast)
import wx.html
if wx.VERSION[0] >= 4:
    import wx.adv
import psycopg2
import datetime
import requests
import re
import getpass
import socket


__progname__ = 'Grukos Uploader'
__authors__ = 'Anders Damsgaard <andam@mst.dk>'
__version__ = '1.2.6'
__repository__ = 'https://gitlab.com/mst-gko/grukos_uploader'
__description__ = __progname__ + ''' creates PostgreSQL templates for
uploading groundwater-related data to the GRUKOS database.'''
__verbose__ = False
__sample_dir__ = os.getcwd() + '/sample_data/'
__template_dir__ = os.getcwd() + '/templates/'
__default_dir__ = ''

__shp_field_types__ = {
    'C': 'text',
    'N': 'number (with or without decimals)',
    'F': 'number (with or without decimals)',
    'L': 'bool',
    'D': 'date',
    'M': 'memo'
}


class DatabaseConnection(object):
    def __init__(self,
                 password,
                 user='grukosreader',
                 host='10.33.131.50',
                 port=5432,
                 database='grukos'):
        self.host = host
        self.port = port
        self.database = database
        self.user = user
        self.password = password
        self.init()

    def __enter__(self):
        return self

    def __exit__(self, ext_type, exc_value, traceback):
        if self.connection is not None:
            self.close()

    def init(self):
        '''
        Connect to the configured database and keep the connection open. Call
        self.close() after usage.
        '''
        self.connection = psycopg2.connect(
            dbname=self.database,
            user=self.user,
            password=self.password,
            host=self.host,
            port=self.port)
        self.cursor = self.connection.cursor()


    def close(self):
        self.cursor.close()
        self.connection.close()

    def upload_shapefile(self, shapefile, table=None, schema='grukosuploader',
                        gui=True):
        '''
        Uploads the selected shapefile to the selected schema in the GRUKOS
        database.

        :param shapefile: Selected shapefile to upload
        :type shapefile: Shapefile
        :param table: Destination table name in GRUKOS database. By default the
            name is generated from the computer hostname, username, shapefile
            file name and the current date and time.
        :type table: str
        :param schema: Destination schema name in GRUKOS database. By default
            set to 'grukosuploader'.
        :type schema: str
        :returns: Destination in database, in the format <schema>, <table>
        :return type: str, str
        '''

        # automatically determine table name if not specified
        if not table:
            table = getpass.getuser() + '-' + \
                    socket.gethostname() + '-' + \
                    shapefile.basename() + \
                    datetime.datetime.now().strftime('-%Y-%m-%d_%H-%M-%S')

        print('Uploading {} as {}.{} to grukos database'.format(shapefile.path,
                                                                schema, table))
        if gui:
            progress_window = wx.ProgressDialog('Uploading shapefile',
                    'Uploading\n{}\n'.format(shapefile.path) +
                    'Large layers may take a long time.')

        # implemented by mimicing shp2pgsql output
        self.cursor.execute('''SET CLIENT_ENCODING to UTF8;
                            SET CLIENT_ENCODING TO UTF8;
                            SET STANDARD_CONFORMING_STRINGS TO ON;''')

        self.cursor.execute('DROP TABLE IF EXISTS "{}"."{}";'
                            .format(schema, table))

        sqlstring = '''BEGIN; CREATE TABLE "{}"."{}"
                            (gid serial, '''.format(schema, table)
        attributes = shapefile.get_attributes()
        i = 0
        for attribute in attributes:
            if i > 0:
                sqlstring += ', '
            i += 1
            sqlstring += '"{}" '.format(attribute[0])
            if attribute[1] == 'C':
                sqlstring += 'VARCHAR({})'.format(attribute[2])
            elif attribute[1] == 'N':
                sqlstring += 'NUMERIC'
            elif attribute[1] == 'F':
                sqlstring += 'FLOAT'
            elif attribute[1] == 'D':
                sqlstring += 'DATE'
            elif attribute[1] == 'L':
                sqlstring += 'BOOLEAN'
            else:
                raise Exception('Error: Encountered unknown field type ' +
                                str(attribute[1]) + ' for ' +
                                str(attribute[0]))

        sqlstring += ');'
        print(sqlstring)
        self.cursor.execute(sqlstring)

        self.cursor.execute('ALTER TABLE "{}"."{}" ADD PRIMARY KEY (gid);'
                            .format(schema, table))

        shapefile.read_attributes()
        records = shapefile.get_records()
        geometry_type = shapefile.filecontents.shapeTypeName
        #geometry_type = records[0].shape.__geo_interface__['type']
        if 'POLYLINE' in geometry_type:
            geometry_type = 'LINESTRING'
        elif 'POLYGON' in geometry_type:
            geometry_type = 'MULTIPOLYGON'

        self.cursor.execute("""SELECT AddGeometryColumn(
            '{}','{}','geom','25832','{}',2);""".format(schema, table,
                                                        geometry_type))

        attributes = shapefile.get_attributes()
        N = shapefile.n_records
        i = 0
        for row in records:
            i += 1

            if gui:
                progress_window.Update(min(100, int(round(float(i)/N * 100))),
                                       'Uploading data\n{}\n\n'
                                       .format(shapefile.path) +
                                       'Large layers may take a long time.')

            sqlstring = 'INSERT INTO "{}"."{}" ('.format(schema, table)
            for i in range(len(attributes)):
                if i > 0:
                    sqlstring += ', '
                sqlstring += '"{}" '.format(attributes[i][0])
            sqlstring += ', geom) VALUES ('
            for i in range(len(attributes)):
                if i > 0:
                    sqlstring += ', '
                sqlstring += "'{}' ".format(row.record[i]).replace("'None'",
                        'null')

            if geometry_type is 'MULTIPOLYGON':
                sqlstring += ", ST_SetSRID(ST_Multi(ST_GeomFromGeoJSON("
            else:
                sqlstring += ", ST_SetSRID(ST_GeomFromGeoJSON("
            sqlstring += "'{}'" \
                    .format(str(row.shape.__geo_interface__)
                            .replace("'", '"')
                            .replace('(', '[')
                            .replace(')', ']')
                            )
            if geometry_type is 'MULTIPOLYGON':
                sqlstring += ')), 25832));'
            else:
                sqlstring += '), 25832));'
            self.cursor.execute(sqlstring)

        #self.cursor.execute('CREATE INDEX "{}_spt_idx"'.format(table)
        #        + ' ON "{}"."{}" USING GIST (geom);'.format(schema, table))

        self.cursor.execute('COMMIT;')
        self.cursor.execute('ANALYZE "{}"."{}";'.format(schema, table))

        if gui:
            progress_window.Update(100, 'Verifying uploaded data...')

        self.cursor.execute('SELECT count(*) FROM "{}"."{}";'
                            .format(schema, table))
        result = self.cursor.fetchone()
        if int(result[0]) != N:
            wx.MessageBox('Uploaded data does not match shapefile',
                          'Error',
                          wx.OK | wx.ICON_ERROR)
            raise Exception('Count on database did not match number of ' +
                            'records in shapefile')

        if gui:
            progress_window.Destroy()
        return schema, table


class FilesystemScanner(object):
    '''
    This class is used to search the filesystem for shapefiles.
    '''

    def __init__(self):
        self.root_dir = __default_dir__
        self.reset_shapefile_lists()

    def reset_shapefile_lists(self):
        '''
        Empty all entries in `self.shapefile_paths` and `self.shapefiles`.
        '''
        self.shapefile_paths = []
        self.shapefiles = []
        self.shapefile_basenames = []

    def set_root_directory(self, path):
        '''
        Set the root directory for the automated search.

        :param path: Path to shapefile on file system
        :type path: str
        '''
        self.root_dir = path

    def get_root_directory(self):
        '''
        Set the root directory for the automated search.

        :returns: Root directory for data files
        :return type: str
        '''
        if self.root_dir == '':
            return 'No directory specified, select one from File menu'
        else:
            return self.root_dir

    def get_basenames(self):
        '''
        Returns the basenames of the found shapefiles.

        :returns: Shapefile basenames with file extension
        :return type: list of str
        '''
        return self.shapefile_basenames

    def find_shapefiles(self, verbose=False):
        '''
        Traverse the root directorectory (`self.root_dir`) and store a list of
        paths to found shapefiles (`*.shp`) in `self.shapefile_paths`.

        :returns: The number of shapefiles found
        :return type: int
        '''
        self.reset_shapefile_lists()

        if verbose:
            print("Finding shapefiles")
        progress_window = wx.ProgressDialog('Searching',
                                            'Found {} shapefiles'
                                            .format(len(self.shapefile_paths)))

        self.shapefile_paths = []
        self.shapefile_basenames = []
        for root, _, files in os.walk(self.root_dir):
            progress_window.Update(min(len(self.shapefile_paths), 100),
                                   'Found {} shapefiles...'
                                   .format(len(self.shapefile_paths)))

            for filename in files:
                if filename.endswith('.shp'):
                    if verbose:
                        print(os.path.join(root, filename))
                    if os.name == 'nt':
                        self.shapefile_paths.append(os.path
                                                    .join(root, filename))
                    else:
                        self.shapefile_paths.append(os.path.join(root,
                                                                 filename))
                    self.shapefile_basenames.append(
                        os.path.basename(os.path.join(root, filename)))

        if verbose:
            print("Initializing shapefiles")
        self.initialize_shapefiles(verbose=verbose)

        if verbose:
            print("Reading shapefile attributes")
            self.print_attributes_for_shapefiles()

        progress_window.Destroy()

    def initialize_shapefiles(self, verbose=False):
        '''
        Initialize shapefiles in `self.shapefile_paths` as `Shapefile` objects,
        stored in `self.shapefiles`.
        '''

        for path in self.shapefile_paths:
            if verbose:
                print(path)
            self.shapefiles.append(Shapefile(path))

    def print_attributes_for_shapefiles(self):
        '''
        Print a shapefile path and attributes to stdout for debugging purposes.
        '''

        for shp in self.shapefiles:
            print(shp.path)
            print(shp.get_attributes())


class Shapefile(object):
    '''
    This class handles operations and references to a single shapefile.

    :param path: Path to a shapefile on disk
    '''

    def __init__(self, path):
        self.path = path
        self.encoding = None
        self.shape_type = None
        self.attribs = None
        self.n_records = None
        self.ok = None
        self.template_match = None
        self.shx_ok = os.path.isfile(path.replace('.shp','.shx'))
        self.read_attributes()

    def __cmp__(self, other):
        '''
        This function is called when two Shapefile objects are compared through
        the `==` operator. Note that the path and number of records are not
        compared. The function will return the attributes of the left-hand side
        object that are not found or different from the object on the
        right-hand side. For most purposes, it will be relevant to have the
        standard template on the left-hand side.

        :returns: 0 if the contents are identical, and a list of differing
            parameters if not.
        :return type: int or list
        '''

        diffs = []
        if self.encoding != other.encoding:
            diffs.append('wrong encoding')
        if self.shape_type != other.shape_type:
            diffs.append('wrong geometry type')

        self_field_names = self.get_attribute_names()
        self_field_names = [x.lower() for x in self_field_names]
        self_field_types = self.get_attribute_types()
        self_field_lengths = self.get_attribute_lengths()
        self_decimal_lengths = self.get_attribute_decimal_lengths()

        other_field_names = other.get_attribute_names()
        other_field_names = [x.lower() for x in other_field_names]
        other_field_types = other.get_attribute_types()
        other_field_lengths = other.get_attribute_lengths()
        other_decimal_lengths = other.get_attribute_decimal_lengths()

        field_names_missing = set(self_field_names).difference(
            other_field_names)
        if len(field_names_missing) > 0:
            diffs.append('missing field names')

        # detect differences in field types
        field_types_incorrect = []

        i = 0
        for fn1 in self_field_names:
            j = 0

            for fn2 in other_field_names:
                if fn1.lower() == fn2.lower():
                    break
                j += 1

            if i < len(self_field_names) and j < len(other_field_names) and \
               self_field_types[i].lower() != other_field_types[j].lower():
                field_types_incorrect.append(fn1)
            i += 1

        if len(field_types_incorrect) > 0:
            diffs.append('incorrect field types')

        if len(diffs) == 0:
            return True, diffs, field_names_missing, field_types_incorrect
        else:
            return False, diffs, field_names_missing, field_types_incorrect

    def basename(self):
        '''
        Returns the file name without the full path.

        :return: file name including extensions
        :return type: str
        '''
        return os.path.basename(self.path)

    def read_attributes(self, encoding=None):
        '''
        Read the attributes of the shapefile on disk and store in
        `self.attribs`.

        :param encoding: Specify the encoding of the shapefile to be read, for
            example 'utf8', 'latin1', or 'ascii'. If no encoding is specified
            (default), the function will try the previously mentioned encodings
            in turn. If everything else fails, the shapefile will be read as
            latin1 with troublesome characters replaced.
        :type encoding: str
        '''

        if encoding:
            encodings = [encoding]
        else:
            encodings = ['utf8', 'latin1', 'ascii', 'utf8-replace']

        for encoding in encodings:
            try:

                if encoding == 'utf8-replace':
                    self.filecontents = shapefile.Reader(self.path,
                                                         encoding='utf8',
                                                         encodingErrors='replace')
                else:
                    self.filecontents = shapefile.Reader(self.path,
                                                         encoding=encoding,
                                                         encodingErrors='replace')

            except UnicodeDecodeError:
                print('Encoding {} invalid for {}'.format(encoding,
                                                          self.path))

            else:
                # file read successfully
                self.encoding = encoding
                shapes = self.filecontents.shapes()
                self.shape_type = self.filecontents.shapeTypeName
                self.attribs = self.filecontents.fields[1:]
                self.n_records = len(self.filecontents.records())
                break

        if not self.encoding:
            self.encoding = 'error'
            print('Could not read ' + self.path)

    def get_attributes(self):
        '''
        Returns the attributes of the shapefile that are read during
        initialization.

        :return: Attributes in shapefile
        :return type: variable, see parameter style
        '''
        return self.attribs

    def get_attribute_names(self):
        '''
        Reads and returns the attribute names of the shapefile.

        :return: Attribute names in shapefile
        :return type: list
        '''
        attributes = self.get_attributes()
        field_names = []
        for attribute in attributes:
            field_names.append(attribute[0])
        return field_names

    def get_attribute_types(self):
        '''
        Reads and returns the attribute types of the shapefile.

        :return: Attribute types in shapefile
        :return type: list
        '''
        attributes = self.get_attributes()
        field_types = []
        for attribute in attributes:
            field_types.append(field_type_lookup(attribute[1]))
        return field_types

    def get_attribute_lengths(self):
        '''
        Reads and returns the attribute lengths of the shapefile.

        :return: Attribute lengths in shapefile
        :return type: list
        '''
        attributes = self.get_attributes()
        field_lengths = []
        for attribute in attributes:
            field_lengths.append(attribute[2])
        return field_lengths

    def get_attribute_decimal_lengths(self):
        '''
        Reads and returns the attribute decimal lengths of the shapefile.

        :return: Attribute lengths in shapefile
        :return type: list
        '''
        attributes = self.get_attributes()
        field_decimal_lengths = []
        for attribute in attributes:
            field_decimal_lengths.append(attribute[3])
        return field_decimal_lengths

    def get_records(self):
        '''
        Returns the geometries and records in the shapefile.
        '''
        shp = shapefile.Reader(self.path, encoding=self.encoding,
                encodingErrors='replace')
        return shp.shapeRecords()


def field_type_lookup(x):
    return __shp_field_types__[x]


class MainScreen(wx.Frame):
    '''
    This class configures the graphical user interface.
    '''

    def __init__(self, *args, **kwargs):
        #self.welcome_splash()
        super(MainScreen, self).__init__(*args, **kwargs)
        self.Maximize()
        self.fs_scanner = FilesystemScanner()
        self.content_not_saved = False
        self.dirname = __default_dir__
        self.template_scanner = FilesystemScanner()
        self.template_scanner.set_root_directory(__template_dir__)
        self.init_ui()
        self.find_template_shapefiles(verbose=__verbose__)
        self.Maximize()

    def welcome_splash(self):
        '''
        Show a welcome message with logo and link to documentation.
        '''
        bitmap = wx.Bitmap('icon.png', wx.BITMAP_TYPE_PNG)

        splash = wx.adv.SplashScreen(bitmap, wx.adv.SPLASH_CENTRE_ON_SCREEN |
                                     wx.adv.SPLASH_NO_TIMEOUT,
                             6000, None, -1, wx.DefaultPosition, wx.DefaultSize,
                             wx.FRAME_NO_TASKBAR | wx.STAY_ON_TOP)
        #wx.Yield()

    def init_ui(self):
        '''
        This function initializes the main window properties and contents.
        '''
        self.panel = wx.Panel(self)

        #self.SetSize((900, 900))
        self.SetTitle(__progname__ + ' v' + __version__)
        self.Show(True)

        self.SetIcon(wx.Icon('icon.ico', wx.BITMAP_TYPE_ICO))

        button_size=(140,24)
        nav_button_size=(80,24)

        sizer1 = wx.GridBagSizer(4, 4)

        # Project root: display and selection
        self.panel.m_static_text1 = wx.StaticText(self.panel, wx.ID_ANY,
                                            'GKO project directory: ',
                                            wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.panel.m_static_text1.Wrap(-1)
        sizer1.Add(self.panel.m_static_text1, pos=(0,0),
                   flag=wx.ALL, border=5)

        self.panel.m_static_text2 = wx.StaticText(self.panel, wx.ID_ANY,
                                            self.fs_scanner
                                                  .get_root_directory(),
                                            wx.DefaultPosition, wx.DefaultSize,
                                            0)
        sizer1.Add(self.panel.m_static_text2, pos=(0,1), span=(2,2),
                   flag=wx.ALL|wx.EXPAND, border=5)

        self.panel.m_button1 = wx.Button(self.panel, wx.ID_ANY,
                                         u"Select directory",
                                   size=button_size)
        sizer1.Add(self.panel.m_button1, pos=(0,3),
                   flag=wx.ALL|wx.ALIGN_RIGHT, border=5)
        self.panel.Bind(wx.EVT_BUTTON, self.set_root, self.panel.m_button1)

        ## Found shapefiles
        self.panel.shp_box_label = wx.StaticBox(self.panel,
                                                label="Shapefile navigation")
        boxsizer1 = wx.StaticBoxSizer(self.panel.shp_box_label, wx.VERTICAL)

        self.panel.shp_selector = wx.Choice(self.panel, wx.ID_ANY)
        boxsizer1.Add(self.panel.shp_selector,
                      flag=wx.ALL|wx.EXPAND|wx.FIXED_MINSIZE,
                      border=5)
        self.panel.shp_selector.Bind(wx.EVT_CHOICE, self.on_select_shp)

        self.panel.shp_result_text = wx.TextCtrl(self.panel,
                                                 style=wx.TE_MULTILINE
                                                 |wx.TE_RICH)
        self.panel.shp_result_text.SetEditable(False)
        boxsizer1.Add(self.panel.shp_result_text, proportion=1,
                      flag=wx.EXPAND|wx.ALL,
                      border=5)

        sizer1.Add(boxsizer1, pos=(2,0), span=(1,2),
                   flag=wx.EXPAND|wx.ALL,
                   border=5)


        ## Shapefile templates
        self.panel.shp_box_label2 = wx.StaticBox(self.panel,
                                                 label="Template navigation")
        boxsizer2 = wx.StaticBoxSizer(self.panel.shp_box_label2, wx.VERTICAL)

        self.panel.shp_selector2 = wx.Choice(self.panel, wx.ID_ANY)
        boxsizer2.Add(self.panel.shp_selector2,
                      flag=wx.ALL|wx.EXPAND|wx.FIXED_MINSIZE,
                      border=5)
        self.panel.shp_selector2.Bind(wx.EVT_CHOICE, self.on_select_shp2)

        self.panel.shp_result_text2 = wx.TextCtrl(self.panel,
                                                  style=wx.TE_MULTILINE
                                                  |wx.TE_RICH)
        self.panel.shp_result_text2.SetEditable(False)
        boxsizer2.Add(self.panel.shp_result_text2, proportion=1,
                      flag=wx.EXPAND|wx.ALL,
                      border=5)

        sizer1.Add(boxsizer2, pos=(2,2), span=(1,2),
                   flag=wx.EXPAND|wx.ALL,
                   border=5)

        # Bottom row
        self.panel.m_static_text_status = wx.StaticText(self.panel, wx.ID_ANY,
                                                        '', wx.DefaultPosition,
                                                        wx.DefaultSize, 0)
        self.panel.m_static_text_status.Wrap(-1)
        sizer1.Add(self.panel.m_static_text_status, pos=(3,0), flag=wx.ALL,
                   border=5)

        self.panel.m_button_upload = wx.Button(self.panel, wx.ID_ANY,
                                               u"Upload", size=button_size)
        sizer1.Add(self.panel.m_button_upload, pos=(3,2),
                   flag=wx.ALL|wx.ALIGN_RIGHT, border=5)
        self.panel.Bind(wx.EVT_BUTTON, self.on_upload,
                        self.panel.m_button_upload)
        self.panel.m_button_upload.Disable()

        self.panel.m_button_close = wx.Button(self.panel, wx.ID_ANY, u"Close",
                                        size=button_size)
        sizer1.Add(self.panel.m_button_close, pos=(3,3),
                   flag=wx.ALL|wx.ALIGN_RIGHT, border=5)
        self.panel.Bind(wx.EVT_BUTTON, self.on_quit, self.panel.m_button_close)

        # Auto-grow two center columns
        sizer1.AddGrowableCol(1, proportion=0)
        sizer1.AddGrowableCol(2, proportion=0)
        sizer1.AddGrowableRow(2)

        # Initialize other window elements
        self.init_menu()
        #self.init_toolbar()

        # Initialize sizing elements
        self.panel.SetSizer(sizer1)
        sizer1.Fit(self)

    def init_menu(self):
        '''
        Set the menu content of the main screen.
        '''
        menubar = wx.MenuBar()

        file_menu = wx.Menu()
        fitem = file_menu.Append(wx.ID_OPEN, 'Select shapefile directory')
        self.Bind(wx.EVT_MENU, self.set_root, fitem)

        fitem = file_menu.Append(wx.ID_ANY, 'Open example shapefiles')
        self.Bind(wx.EVT_MENU, self.set_root_to_samples, fitem)
        file_menu.AppendSeparator()

        fitem = file_menu.Append(wx.ID_ANY, 'Show shapefile templates')
        self.Bind(wx.EVT_MENU, self.open_templates_in_explorer, fitem)
        file_menu.AppendSeparator()

        fitem = file_menu.Append(wx.ID_EXIT, 'Quit', 'Quit application')
        self.Bind(wx.EVT_MENU, self.on_quit, fitem)
        menubar.Append(file_menu, '&File')


        help_menu = wx.Menu()

        fitem = help_menu.Append(wx.ID_ANY, 'MST-GKO webpage',
                                 'Open MST-GKO webpage')
        self.Bind(wx.EVT_MENU, self.on_mst_gko_page, fitem)

        help_menu.AppendSeparator()

        fitem = help_menu.Append(wx.ID_ANY, 'Check for updates',
                                 'Check for updates')
        self.Bind(wx.EVT_MENU, self.on_check_for_updates, fitem)

        fitem = help_menu.Append(wx.ID_ANY, 'Download page',
                                 'Open download page')
        self.Bind(wx.EVT_MENU, self.on_download_page, fitem)

        fitem = help_menu.Append(wx.ID_ANY, 'Issue tracker',
                                 'Open online issue tracker')
        self.Bind(wx.EVT_MENU, self.on_issue_tracker, fitem)

        fitem = help_menu.Append(wx.ID_HELP, 'Help', 'Help')
        self.Bind(wx.EVT_MENU, self.on_help, fitem)

        help_menu.AppendSeparator()

        fitem = help_menu.Append(wx.ID_ABOUT, 'About', 'About')
        self.Bind(wx.EVT_MENU, self.on_about, fitem)
        menubar.Append(help_menu, '&Help')

        self.SetMenuBar(menubar)

    def init_toolbar(self):
        '''
        Initialize the toolbar for the main window.
        '''

        toolbar = self.CreateToolBar()
        qtool = toolbar.AddLabelTool(wx.ID_EXIT, 'Quit',
                                     wx.Bitmap('textit.png'))
        self.Bind(wx.EVT_TOOL, self.on_quit, qtool)

        toolbar.Realize()

    def on_quit(self, event):
        '''
        Set actions when the application is quit. This function will check for
        unsaved changes.
        '''

        if self.content_not_saved:
            if wx.MessageBox('Current upload template is not saved! Proceed?',
                             'Please confirm',
                             wx.ICON_QUESTION | wx.YES_NO, self) == wx.NO:
                return
        self.Close()

    def set_root(self, event):
        '''
        Set root directory containing shapefiles related to the project.
        '''
        open_dir_dialog = wx.DirDialog(self, 'Choose project directory',
                                       self.dirname,
                                       wx.DD_DEFAULT_STYLE |
                                       wx.DD_DIR_MUST_EXIST)

        if open_dir_dialog.ShowModal() == wx.ID_OK:
            self.dirname = open_dir_dialog.GetPath()

        open_dir_dialog.Destroy()

        self.fs_scanner.set_root_directory(self.dirname)
        self.panel.m_static_text2.SetLabel(self.dirname)
        self.find_shapefiles(None, verbose=__verbose__)

    def set_root_to_samples(self, event):
        '''
        Set root directory to supplied shapefile samples.
        '''
        self.dirname = __sample_dir__
        self.fs_scanner.set_root_directory(self.dirname)
        self.panel.m_static_text2.SetLabel(self.dirname)
        self.find_shapefiles(None, verbose=__verbose__)

    def open_templates_in_explorer(self, event):
        '''
        Open the template directory in an external file explorer
        '''

        path = __template_dir__
        if os.name == 'nt':
            path.replace('/','\\')
            subprocess.Popen('explorer /select,{}'.format(path)) 

        elif os.name == 'posix':
            subprocess.call(['xdg-open', path]) 

        elif os.name == 'mac':
            subprocess.call(['open', path]) 

        else:
            wx.MessageBox('Unknown operating system, could not open external' +
                          ' file explorer',
                          'Error',
                          wx.OK | wx.ICON_ERROR)
            raise Exception('Unknown operating system')


    def find_shapefiles(self, event, find_template_matches=True, verbose=False):
        '''
        Find shapefiles contained in the root directory and its subdirectories,
        and update the user interface accordingly.

        :param find_template_matches: Attempt to match found shapefiles against
            found shapefile templates
        :type find_template_matches: Bool
        '''
        if self.fs_scanner.root_dir == '':
            wx.MessageBox('No input directory specified',
                          'Error',
                          wx.OK | wx.ICON_ERROR)
            return

        self.fs_scanner.find_shapefiles(verbose=verbose)

        if len(self.fs_scanner.shapefile_paths) < 1:
            wx.MessageBox('No shapefiles found in\n' +
                          self.fs_scanner.get_root_directory(),
                          'Warning',
                          wx.OK | wx.ICON_WARNING)

        self.panel.shp_box_label.SetLabel('Shapefile navigation ({} files)'
                                    .format(len(self.fs_scanner.shapefiles)))

        self.panel.shp_selector.SetItems(self.fs_scanner.get_basenames())
        self.panel.shp_selector.SetSelection(0)

        if find_template_matches:
            # Try matching by basename first, then attributes
            self.find_template_matches(method='basename')
            self.find_template_matches(method='attributes')

        # Compare content of shapefiles
        for shapefile in self.fs_scanner.shapefiles:
            if shapefile.template_match is not None:
                self.compare_shps(shapefile,
                                  self.template_scanner.shapefiles[
                                      shapefile.template_match])
            else:
                shapefile.ok = False
        self.report_status()

        self.on_select_shp(None)

    def report_status(self):
        '''
        Report top-level status of found shapefiles to the main window

        :returns: The number of shapefiles that are not valid
        :return type: int
        '''
        n_ok = 0
        for shapefile in self.fs_scanner.shapefiles:
            if shapefile.ok is None:
                raise Exception('Shapefile status not checked')
            if shapefile.ok:
                n_ok = n_ok + 1

        n_not_ok = len(self.fs_scanner.shapefiles) - n_ok

        self.panel.m_static_text_status.SetLabel('Shapefile status: ' +
                                                 '{} OK, {} not OK'
                                                 .format(n_ok, n_not_ok))
        return n_not_ok

    def find_template_matches(self, method='attributes'):
        '''
        Find matches between found shapefiles in the project directory and the
        detected shapefile templates. The best match is stored as an integer
        value `template_match` in the Shapefile objects in the `fs_scanner`
        FilesystemScanner object.

        ## Matching methods
        ### basename
        This method compares the basename of the shapefile and the template
        shapefiles. If two are identical, the shapefile is assumed to
        correspond to the template. This method is relatively fast.

        ### attributes
        This method compares the attributes of the shapefile and the template
        shapefiles. If the two attribute sets are identical, the shapefile is
        assumed to correspond to the template. This method is relatively slow
        as each shapefile is compared against all templates.

        ## Parameters
        :param method: Matching method, valid options are 'basename' (default)
            or 'attributes'
        :type method: str
        '''
        template_basenames = self.template_scanner.get_basenames()
        for shapefile in self.fs_scanner.shapefiles:

            # Skip if shapefile already has a registered match
            if shapefile.template_match is not None:
                continue

            try:
                if method == 'basename':
                    shapefile.template_match = \
                            template_basenames.index(shapefile.basename()
                                                     .lower())

                elif method == 'attributes':
                    i = 0
                    for shapefile_template in self.template_scanner.shapefiles:
                        result, _, _ = shapefile_template.__cmp__(shapefile)
                        if result:
                            shapefile.template_match = i
                        i += 1

                else:
                    raise Exception('Matching method "{}" not valid'
                                    .format(method))

                print(u'Match {} found for {}'
                      .format(shapefile.template_match, shapefile.path))

            except ValueError:
                print(u'No match found for {}'.format(shapefile.path))
                shapefile.ok = False

    def find_template_shapefiles(self, verbose=False):
        '''
        Find shapefiles contained in the root directory and its subdirectories,
        and update the user interface accordingly.
        '''
        self.template_scanner.find_shapefiles(verbose=verbose)

        if len(self.template_scanner.shapefile_paths) < 1:
            wx.MessageBox('No shapefiles found in\n' +
                          self.template_scanner.get_root_directory(),
                          'Error',
                          wx.OK | wx.ICON_ERROR)

        self.panel.shp_box_label2.SetLabel('Template navigation ({} files)'
                                     .format(len(
                                         self.template_scanner.shapefiles)))

        self.panel.shp_selector2.SetItems(self.template_scanner.get_basenames())
        self.panel.shp_selector2.SetSelection(0)
        self.on_select_shp2(None)

    def on_select_shp(self, event):
        '''
        This function is called when a shapefile is selected from the shapefile
        selector (`self.shp_selector`).
        '''

        self.panel.shp_box_label.SetLabel('Shapefile navigation ({} of {})'
                                    .format(self.panel.shp_selector.GetSelection()+1,
                                            len(self.fs_scanner.shapefiles)))
        shapefile = self.current_shapefile()

        self.display_shp_attributes(shapefile, self.panel.shp_result_text) 

        if shapefile.template_match:
            self.panel.shp_selector2.SetSelection(shapefile.template_match)
            self.on_select_shp2(None)

        else:
            if len(self.fs_scanner.shapefiles) > 0 and \
               len(self.template_scanner.shapefiles) > 0:
                self.compare_shps(
                    self.fs_scanner.shapefiles[
                        self.panel.shp_selector.GetSelection()],
                    self.template_scanner.shapefiles[
                        self.panel.shp_selector2.GetSelection()])
        self.panel.m_button_upload.Enable()

    def on_select_shp2(self, event):
        '''
        This function is called when a shapefile is selected from the shapefile
        selector (`self.shp_selector`).
        '''

        self.panel.shp_box_label2.SetLabel('Template navigation ({} of {})'
                                    .format(self.panel.shp_selector2
                                            .GetSelection()+1,
                                            len(self.template_scanner
                                                .shapefiles)))
        self.display_shp_attributes(
            self.template_scanner.shapefiles[
                self.panel.shp_selector2.GetSelection()],
            self.panel.shp_result_text2) 
        if len(self.fs_scanner.shapefiles) > 0:
            self.display_shp_attributes(
                self.fs_scanner.shapefiles[
                    self.panel.shp_selector.GetSelection()],
                self.panel.shp_result_text) 

        if len(self.fs_scanner.shapefiles) > 0 and \
           len(self.template_scanner.shapefiles) > 0:
            self.compare_shps(
                self.fs_scanner.shapefiles[
                    self.panel.shp_selector.GetSelection()],
                self.template_scanner.shapefiles[
                    self.panel.shp_selector2.GetSelection()])

    def select_prev_shp(self, event):
        '''
        This function selects the previous shapefile in the drop-down>
        '''
        if self.panel.shp_selector.GetSelection() > 0:
            self.panel.shp_selector.SetSelection(self.panel.shp_selector.GetSelection()-1)
            self.on_select_shp(None)

    def select_next_shp(self, event):
        '''
        This function selects the next shapefile in the drop-down>
        '''
        if self.panel.shp_selector.GetCount() > \
           self.panel.shp_selector.GetSelection()+1:
            self.panel.shp_selector.SetSelection(
                self.panel.shp_selector.GetSelection()+1)
            self.on_select_shp(None)

    def compare_shps(self, shp, shp_template):
        '''
        Compare two Shapefile objects.
        '''
        result, diffs, field_names_missing, field_types_incorrect = \
                shp_template.__cmp__(shp)

        if not shp.shx_ok:
            result = False
            diffs.append('Shapefile .shx file is missing')

        if result:
            shp.ok = True
            self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.GREEN))
            self.panel.shp_result_text.AppendText('\nFILE OK')
            self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.BLACK))
        else:
            shp.ok = False
            self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.RED))
            self.panel.shp_result_text.AppendText('\nFILE NOT OK\n')
            self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.BLACK))

            if len(diffs) > 1:
                self.panel.shp_result_text.AppendText('\nReasons: ')
            else:
                self.panel.shp_result_text.AppendText('\nReason: ')

            i = 0
            for diff in diffs:
                i = i + 1
                self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.RED))
                self.panel.shp_result_text.AppendText('{}'.format(diff))
                self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                if i < len(diffs):
                    self.panel.shp_result_text.AppendText(', ')
                else:
                    self.panel.shp_result_text.AppendText('\n')

            if 'missing field names' in diffs:
                if len(field_names_missing) > 1:
                    self.panel.shp_result_text.AppendText('\nMissing fields: ')
                else:
                    self.panel.shp_result_text.AppendText('\nMissing field: ')

                i = 0
                for field_name in field_names_missing:
                    i = i + 1
                    self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.RED))
                    self.panel.shp_result_text.AppendText('{}'.format(field_name))
                    self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                    if i < len(field_names_missing):
                        self.panel.shp_result_text.AppendText(', ')
                    else:
                        self.panel.shp_result_text.AppendText('\n')

            if 'incorrect field types' in diffs:
                if len(field_types_incorrect) > 1:
                    self.panel.shp_result_text.AppendText(
                        '\nFields with incorrect type: ')
                else:
                    self.panel.shp_result_text.AppendText(
                        '\nField with incorrect type: ')

                i = 0
                for field_name in field_types_incorrect:
                    i = i + 1
                    self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.RED))
                    self.panel.shp_result_text.AppendText('{}'.format(field_name))
                    self.panel.shp_result_text.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                    if i < len(field_names_missing):
                        self.panel.shp_result_text.AppendText(', ')
                    else:
                        self.panel.shp_result_text.AppendText('\n')

        if shp.template_match:
            self.panel.shp_result_text.AppendText('\n\nMatching template found ')
            self.panel.shp_result_text.AppendText('({})'.format(
                self.template_scanner.shapefiles[shp.template_match].basename()))
        else:
            self.panel.shp_result_text.AppendText('\nMatching template not found ')


    def display_shp_attributes(self, shp, text_field):
        '''
        Display the shapefile attributes in the main text field.

        :param shp: Shapefile to display.
        :type shp: Shapefile
        '''
        text_field.SetValue(u'\nBasename: {}\n\n'.format(shp.basename()))
        #text_field.AppendText(u'Full path: {}\n\n'.format(shp.path))
        text_field.AppendText('Encoding: {}\n'
                              .format(shp.encoding))

        text_field.AppendText('Geometry type: {}\n\n'
                              .format(shp.shape_type))

        field_names = shp.get_attribute_names()
        field_types = shp.get_attribute_types()
        field_lengths = shp.get_attribute_lengths()
        decimal_lengths = shp.get_attribute_decimal_lengths()

        for i in range(len(field_names)):

            text_field.AppendText(' - ')
            text_field.SetDefaultStyle(wx.TextAttr(wx.BLACK))
            text_field.AppendText('{} ('.format(field_names[i]))
            text_field.SetDefaultStyle(wx.TextAttr(wx.BLUE))
            text_field.AppendText('{}'.format(field_types[i]))
            text_field.SetDefaultStyle(wx.TextAttr(wx.BLACK))
            text_field.AppendText('): length: {}, precision: {}\n'
                                  .format(field_lengths[i],
                                          decimal_lengths[i]))

        text_field.AppendText('\nNumber of records: {}\n'
                              .format(shp.n_records))

    def current_shapefile(self):
        '''
        Returns the currently selected shapefile in the left main pane.

        :returns: Selected shapefile
        :return type: Shapefile
        '''
        return self.fs_scanner.shapefiles[
            self.panel.shp_selector.GetSelection()]

    def on_upload(self, event):
        self.panel.m_button_upload.Disable()
        schema = None
        table = None
        with DatabaseConnection('gebr470ne') as dbc:
            shapefile = self.current_shapefile()
            print(shapefile.path)
            try:
                schema, table = dbc.upload_shapefile(shapefile)
            except Exception as err:
                dlg = wx.MessageDialog(self,
                                       'Error during upload\n\n' + str(err),
                                       'Upload error',
                                       wx.OK | wx.ICON_ERROR)
                self.panel.m_button_upload.Enable()
                retval = dlg.ShowModal()
        if schema and table:
            dlg = wx.MessageDialog(self,
                                   'Shapefile contents uploaded to\n' +
                                   '"{}"."{}"\n'
                                   .format(schema, table) + 
                                   'in the GRUKOS database.',
                                   'Upload finished',
                                   wx.OK | wx.ICON_INFORMATION)
            retval = dlg.ShowModal()
            self.panel.m_button_upload.Enable()
        else:
            dlg = wx.MessageDialog(self,
                                   'Could not upload shapefile to GRUKOS.\n' +
                                   'Is this machine on the MST-GKO network?',
                                   'Upload error',
                                   wx.OK | wx.ICON_ERROR)
            self.panel.m_button_upload.Enable()
            retval = dlg.ShowModal()

    def on_mst_gko_page(self, event):
        open_url_in_browser('https://mst.dk/gko')

    def on_check_for_updates(self, event):
        newest_version = get_newest_release()
        if __version__ != newest_version:
            print('Not newest version: ' + newest_version)
            dlg = wx.MessageDialog(self,
                                   'Your version ({}) '.format(__version__) +
                                   'is not the newest available ({}).\n\n'
                                   .format(newest_version) +
                                   'Please go to "Help > Download page"' +
                                   'to update.',
                                   'Version out of date',
                                   wx.OK | wx.ICON_WARNING)
            retval = dlg.ShowModal()
        else:
            print('Newest version: ' + newest_version)
            dlg = wx.MessageDialog(self,
                                   'Your version ({}) '.format(__version__) +
                                   'is up to date.',
                                   'Information',
                                   wx.OK | wx.ICON_INFORMATION)
            retval = dlg.ShowModal()

        dlg.Destroy()

    def on_download_page(self, event):
        open_url_in_browser(__repository__ + '/blob/master/README.md')

    def on_issue_tracker(self, event):
        open_url_in_browser(__repository__ + '/issues')

    def on_help(self, event):
        '''
        Show included html documentation as help dialog. It is chosen not to
        use the Microsoft HTML Help Workshop framework.
        '''
        frm = HelpFrame(None, 'Help Browser')
        frm.Show()

    def on_about(self, event):
        '''
        Show about screen
        '''

        license_text = __progname__ + ''' is free software; you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details. You should have recieved a copy of the GNU General
Public License along with ''' + __progname__ + '''; if not, write to the Free
Software Foundation, Inc., 59 Temple Palace, Suite 330, Boston, MA
02111-1307  USA'''

        if wx.VERSION[0] >= 4:
            info = wx.adv.AboutDialogInfo()
        else:
            info = wx.AboutDialogInfo()

        info.SetName(__progname__)
        info.SetVersion(__version__)
        info.SetDescription(__description__)
        info.SetCopyright(u'(C) 2018 Miljøstyrelsen ' +
                          '(Danish Environmental Protection Agency)')
        info.SetWebSite(__repository__)
        info.SetLicense(license_text)
        info.AddDeveloper(__authors__)
        info.AddDocWriter(__authors__)
        info.AddArtist(__authors__)

        if wx.VERSION[0] >= 4:
            wx.adv.AboutBox(info)
        else:
            wx.AboutBox(info)


class HelpFrame(wx.Frame):
    '''
    Initializes a GUI frame with HTML rendering for displaying the help in the
    'html/' directory.
    '''
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, -1, title, size = (600,400))
        self.html = wx.html.HtmlWindow(self, style=wx.html.HW_SCROLLBAR_AUTO)
        self.html.Bind(wx.html.EVT_HTML_LINK_CLICKED, self.on_link_click)

        if "gtk2" in wx.PlatformInfo:
            self.html.SetStandardFonts()

        filename = os.getcwd() + '/help/index.html'
        if os.path.exists(filename):
            self.html.LoadPage(filename)

        else:
            dlg = wx.MessageDialog(self, '''Unable to locate help files
  \nThe files should be located here: 
  {}\n\nbut were not found.'''.format(os.path.dirname(filename)),
                                     'Help Files Not Found',
                                     wx.OK | wx.ICON_EXCLAMATION )
            retval=dlg.ShowModal()
            dlg.Destroy()

    def on_link_click(self, event):
        '''
        Set actions when a hyperlink is clicked.
        '''

        link_info = event.GetLinkInfo()
        href = link_info.GetHref()
        if href == '__CLOSE__':
            self.Close()
        else:
            filename = os.getcwd() + '/help/' + href
            if os.path.exists(filename):
                self.html.LoadPage(filename)

            else:
                dlg = wx.MessageDialog(self, u'''Unable to open help file:
                                       \n{}'''.format(
                                           os.path.dirname(filename)),
                                         "Help File Not Found",
                                         wx.OK | wx.ICON_EXCLAMATION )
                retval=dlg.ShowModal()
                dlg.Destroy()


def open_url_in_browser(url):
    '''
    Open an URL in an external program.

    :param url: A web address
    :type url: str
    '''
    if os.name == 'nt':
        opencmd = 'explorer'

    elif os.name == 'posix':
        opencmd = 'xdg-open'

    elif os.name == 'mac':
        opencmd = 'open'

    else:
        wx.MessageBox('Unknown operating system, could not open external' +
                      ' browser',
                      'Error',
                      wx.OK | wx.ICON_ERROR)
        raise Exception('Unknown operating system')

    subprocess.call([opencmd, url]) 

def get_newest_release():
    '''
    Get the version number of the newest release tagged on the project
    repository.

    :returns: Version string
    :return type: str
    '''

    tag_page = requests.get(__repository__ + '/tags')
    if not tag_page.ok:
        print("Error: Could not connect to repository")
        print("Code {}".format(tag_page.status_code))
        print(tag_page.text)
    return re.findall('v\d+\.\d+\.\d+', tag_page.text)[0][1:]

def parse_command_line_arguments():
    '''
    :returns: Parsed command line arguments
    :type: argparse.Namespace
    '''
    parser = argparse.ArgumentParser(description=__description__)

    parser.add_argument('-V', '--verbose', action='store_true',
                        help='show verbose output during execution')

    parser.add_argument('-t', '--test', action='store_true',
                        help='run unit tests instead of starting normally')

    parser.add_argument('-v', '--version', action='version',
                        version='''
                        %(prog)s {}. Copyright (C) 2018 Miljoestyrelsen, License
                        GPLv3 GNU GPL version 3 or later
                        <http://gnu.org/licenses/gpl.html>.
                        This is free software: you are free to change and
                        redistribute it. There is NO WARRANTY, to the extent
                        permitted by law.
                        Written by {}. Code repository: {}
                        '''.format(__version__, __authors__, __repository__))

    args = parser.parse_args()

    return args


def main():
    '''
    Open main GUI
    '''
    app = wx.App()
    ms = MainScreen(None)
    ms.Show()
    app.MainLoop()


def test_main():
    '''
    Run unit tests
    '''
    pass


if __name__ == '__main__':
    # Call main function if the program is initialized by itself
    ARGS = parse_command_line_arguments()
    __verbose__ = ARGS.verbose
    if ARGS.test:
        test_main()
    else:
        main()
