REM Run this script on a 64-bit Windows machine in a Anaconda Python 3 prompt

cd C:\gitlab\grukos_uploader

echo ### creating new conda virtualenv called exe ###
call conda create -n exe --yes python=3
call conda activate exe
pip install https://github.com/pyinstaller/pyinstaller/archive/develop.zip
pip install pyshp wxPython dis3 requests psycopg2

echo ### Removing previous build directory ###
if exist "grukos_uploader\" rmdir /S /Q grukos_uploader
if exist "build\" rmdir /S /Q build
if exist "dist\" rmdir /S /Q dist
if exist "grukos_uploader.zip" del "*.zip" /s /f /q

echo ### Build stand-alone application ###
pyinstaller --onefile --windowed grukos_uploader.py --icon icon.ico

echo ### Copy auxillary files to build directory ###
if not exist "grukos_uploader\" mkdir "grukos_uploader"
copy dist\grukos_uploader.exe grukos_uploader\
copy LICENSE grukos_uploader\
copy icon.ico grukos_uploader\
copy README.md grukos_uploader\
md grukos_uploader\templates
xcopy /S /E "F:\GKO\data\grukos\revideret_dataaflevering\Skabelon_nummer_GKO navn" grukos_uploader\templates
md grukos_uploader\sample_data
xcopy /S /E sample_data grukos_uploader\sample_data
md grukos_uploader\help
xcopy /S /E help grukos_uploader\help

echo ### Zipping folder ###
SET sevenzip_path="C:\Program Files\7-Zip\7z.exe"
%sevenzip_path% a -tzip "C:\gitlab\grukos_uploader\grukos_uploader.zip" "C:\gitlab\grukos_uploader\grukos_uploader"

echo ### Cleaning up repo ###
rmdir /S /Q dist
rmdir /S /Q build
rmdir /S /Q __pycache__
del "*.spec" /s /f /q
rmdir /S /Q grukos_uploader

echo ### Removing conda virtualenv called exe ###
call conda activate base
call conda env remove -n exe
