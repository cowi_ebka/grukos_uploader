# Grukos Uploader

This application is an interactive tool for verifying groundwater-related data 
from the local file system before it is submitted to the GRUKOS database. When 
used on computers on the MST-GKO network, the program can upload shapefile 
contents to the GRUKOS database.

## Download
The latest stable release **v1.2.6** can be downloaded here:

- [Windows build](https://gitlab.com/mst-gko/grukos_uploader/-/raw/master/grukos_uploader.zip)

**Tip**: You can check if your Grukos Uploader is up to date by choosing *Check 
for updates* in the *Help* menu.

## Installation
Extract the zip file downloaded through the links above into an appropriate 
folder, and execute or double-click the `grukos_uploader.exe` binary file. No 
administrative credentials are required to install or run the program.

**NOTE**: Since Grukos Uploader is a new program, you may encounter anti-virus 
warnings on the first launch.

## Help
Please see the included documentation, available from the "Help" menu within 
the application, or from the [help](help/index.html) directory.

## Development
Clone this project to do active development on the code base. Start the program 
with `python grukos_uploader.py` to see diagnostic output to the console. 

The windows build is conducted manually by running the batch file 
'make_win_build.bat' within an anaconda termanal. The batch file is 
creating a virtual python environment and building the windowsprogram 
using pyinstaller. The batchfile is fetching the newest templates used within
the program when building the program.

The internal update mechanism in the program (*Help* > *Check for updates*) 
looks for the latest tagged release on the [Gitlab
CI](https://gitlab.com/mst-gko/grukos_uploader) page, and compares it to its 
own version.

## Author and License
Grukos Uploader is written by [Anders Damsgaard](mailto:jakla@mst.dk) and 
maintained by [Jakob Lanstorp](mailto:jakla@mst.dk) and [Simon Makwarth](mailto:simak@mst.dk). 
Licensed under the GNU Public License version 3 or later. See 
[LICENSE](LICENSE) for details.
